import { Component } from '@angular/core';
import { PhonenumberPipe } from './phonenumber.pipe';
import { DatatableComponent } from './datatable/datatable.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
