import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) {
        this.getJSON().subscribe(data => {
            //console.log(data)
        });
    }

    public getJSON(): Observable<any> {
        return this.http.get("./assets/data.json")
    }
    

    public postData(id:number,status:string){
        console.log(id+status);
        const req = this.http.post('https://facebook.com', {
      resid: id,
      resStatus: status
    })
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log("Error occured");
        }
      );
    }
}
