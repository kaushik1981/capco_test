import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DatatableComponent } from './datatable/datatable.component';
import { HttpClientModule} from '@angular/common/http'; 
import {DataService} from './shared/data.service';
import {PagingService} from './shared/paging.service';
import { PhonenumberPipe } from './phonenumber.pipe';
import {HttpModule} from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    DatatableComponent,
    PhonenumberPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule
    
  ],
  providers: [DataService,PagingService,PhonenumberPipe],
  bootstrap: [AppComponent],
  //schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
