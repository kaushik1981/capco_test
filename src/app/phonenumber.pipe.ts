import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phonenumber'
})
export class PhonenumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value.length<=8)
       value= "0-000-" + value;
    return value;
  }

}
