import { Component, OnInit } from '@angular/core';
import {DataService} from '../shared/data.service';
import {HttpErrorResponse} from '@angular/common/http';
import {PagingService} from '../shared/paging.service';

import { PhonenumberPipe } from '../phonenumber.pipe';
import { HttpClientModule} from '@angular/common/http'; 
@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css'],
  
})
export class DatatableComponent implements OnInit {

  constructor(private dataService:DataService, private pagerService: PagingService) { }
  
  // array of all items to be paged
    private allItems: any[];
 
    // pager object
    pager: any = {};
 
    // paged items
    pagedItems: any[];

    pageSize : any;
 
  ngOnInit() {
    
    this.dataService.getJSON().subscribe(data => {
            this.allItems = data;
            this.setPage(1,this.pageSize);
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          });
  }

  setPage(page: number, pagesize:number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
 
        // get pager object from service
        
        this.pager = this.pagerService.getPager(this.allItems.length, page,pagesize);
        console.log(this.pager);
        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    onChange(event:any){
      this.pager.currentpage= 1;
      console.log("-----"+ event);
      let page = parseInt(event,10);
      this.setPage(this.pager.currentpage,page);
    }

    postResponse(id:number,status:string){
      this.dataService.postData(id,status);
    }

}
